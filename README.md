# ZeNeuSound Survey

## English
First of all, thank you for taking the time to test our sonification software, ZeNeuSound, and providing feedback.
Before going further, please ensure you have downloaded the two folder (Data and Matlab scripts) as well as the .zip file. Please make sure to unzip the zip file, in which you will find the software.
In this repository you will find all the resources needed to pursue the survey.

The google form is accessible through these links :
- https://forms.gle/MAni8ZP9bHMdpS9i7 (expert users, english)
- https://forms.gle/8xgyS69gRS6uzMDt5 (regular users, english)

### Data 
This folder contains the data to sonify : the regions of interest (ROIs), the fluorescence signals, the videos and the clusters. These are formatted to be directly used in the software.

### Matlab scripts
This folder contains the scripts needed to format your raw data in order to load them in the software

- _exportClusters.m_ : clusters files.
- _exportROI_txt.m_ : FIJI ROIs files.
- _read_resultsImageJ.m_ : averaged fluorescence signals.

### Patcher.zip
This archive contains the software itself. In order to lauche the patcher, please unzip the file and double click on the .exe.

Thanks a lot for your participation !

## Français
Tout d'abord, merci de prendre le temps de tester notre logiciel de sonification, ZeNeuSound, et de nous faire part de vos commentaires.
Dans ce repository, vous trouverez toutes les ressources nécessaires pour poursuivre l'enquête.
Avant d'aller plus loin, merci de vous assurer de télécharger les deux dossiers (Data et MAtlab script), et de décompresser l'archive Patcher.zip. dans laquelle se trouve le logiciel.

Le formulaire google est accessible à travers ces liens :
- https://forms.gle/5z3U67x6jSCZQhqo9 (utilisateurs experts, français)
- https://forms.gle/53CcBvKi49M1tqQD9 (utilisateurs non experts, français)

### Données 
Ce dossier contient les données à sonifier : les régions d'intérêt (ROIs), les signaux de fluorescence, les vidéos et les clusters. Ces données ont été formatées pour être utilisées directement dans le logiciel.

### Scripts Matlab
Ce dossier contient les scripts nécessaires pour formater vos données brutes afin de les charger dans le logiciel.

- _exportClusters.m_ : fichiers de clusters.
- _exportROI_txt.m_ : fichiers FIJI ROIs.
- _read_resultsImageJ.m_ : signaux de fluorescence moyennés.

### Patcher.zip
Cette archive contient le logiciel lui-même. Pour lancer le patcheur, veuillez décompresser le fichier et double-cliquer sur le .exe.

Merci beaucoup pour votre participation !
