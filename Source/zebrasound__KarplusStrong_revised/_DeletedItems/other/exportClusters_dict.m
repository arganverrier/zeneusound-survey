clear all, close all

load('workspace', 'ClusterGroups');
fileid=fopen('../data/clusters_explant2.json', 'w');

one=ClusterGroups.one.';
two=ClusterGroups.two.';
three=ClusterGroups.three.';
four=ClusterGroups.four.';
five=ClusterGroups.five.';

fprintf(fileid, '%s\n', "{");

for neuron = one
    str = int2str(neuron);
    fprintf(fileid, '\t"%s" : 1,\n', str);
end

for neuron = two
    str = int2str(neuron);
    fprintf(fileid, '\t"%s" : 2,\n', str);
end

for neuron = three
    str = int2str(neuron);
    fprintf(fileid, '\t"%s" : 3,\n', str);
end

for neuron = four
    str = int2str(neuron);
    fprintf(fileid, '\t"%s" : 4,\n', str);
end

for neuron = five(1:length(five)-1)
    str = int2str(neuron);
    fprintf(fileid, '\t"%s" : 5,\n', str);
end
neuron = five(length(five));
str = int2str(neuron);
fprintf(fileid, '\t"%s" : 5\n', str);
fprintf(fileid, '%s\n', "}");

fclose(fileid)